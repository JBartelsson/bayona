using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MagicStone))]
public class MagicStoneRadiusEditor : Editor
{

	void OnSceneGUI()
	{
		MagicStone fow = (MagicStone)target;
		Handles.color = Color.white;
		Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.radius/2);
		
    }

}