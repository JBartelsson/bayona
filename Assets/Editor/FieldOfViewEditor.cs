using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(EnemyEditorView))]
public class FieldOfViewEditor : Editor
{

	void OnSceneGUI()
	{
		EnemyEditorView parent = (EnemyEditorView)target;
		FieldOfView fow = parent.transform.GetComponentInChildren<FieldOfView>();
		Handles.color = Color.white;
		Vector3 pos = fow.transform.position - (Vector3.up * fow.transform.localScale.y);
		Handles.DrawWireArc(pos, Vector3.up, Vector3.forward, 360, fow.viewRadius);
		Vector3 viewAngleA = fow.DirectionFromAngle(-fow.viewAngle / 2, false);
		Vector3 viewAngleB = fow.DirectionFromAngle(fow.viewAngle / 2, false);

		Handles.DrawLine(pos, pos + viewAngleA * fow.viewRadius);
		Handles.DrawLine(pos, pos + viewAngleB * fow.viewRadius);

		Handles.color = Color.red;
        foreach (Transform visibleTarget in fow.visibleTargets)
        {
            Handles.DrawLine(fow.transform.position, visibleTarget.position);
        }
    }

}