using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIdleStateWait : EnemyBaseState
{
    public float waitTime;
    float timer;

    private void Start()
    {

    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        enemy.animator.SetBool("EnemyMove", false);
        timer = 0;
    }


    public override void UpdateState(EnemyStateAgent enemy)
    {
        timer += Time.deltaTime;
        if (timer > waitTime)
        {
            enemy.SwitchState(enemy.patrolState);
        }
    }
}
