using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyWatchState : EnemyBaseState
{
    NavMeshAgent agent;
    float radius;
    List<Transform> targets;
    Transform target;

    int oldLength;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        enemy.animator.SetBool("EnemyMove", true);
        radius = enemy.fov.viewRadius;
        targets = enemy.fov.visibleTargets;
        isPlayerInReach();
        oldLength = targets.Count;
        Debug.Log("Hello from Watch");
    }

    void isPlayerInReach()
    {
        target = targets[0];
        foreach (var t in targets)
        {
            if (t.CompareTag("Player"))
            {
                target = t;
            }
        } 
    }
    public override void UpdateState(EnemyStateAgent enemy)
    {
        targets = enemy.fov.visibleTargets;
        if (targets.Count != 0)
        {
            
            if (targets.Count != oldLength)
            {
                isPlayerInReach();
                oldLength = targets.Count;
            }
            if (Vector3.Distance(transform.position, target.position) < enemy.fov.viewRadius / 2)
            {
                transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
            } else
            {
                Vector3 point = target.position + (transform.position - target.position).normalized * enemy.fov.viewRadius / 2;
                agent.destination = point;
                Debug.DrawLine(transform.position, point, Color.white);
            }

        } else
        {
            enemy.SwitchState(enemy.patrolState);
        }
    }
}
