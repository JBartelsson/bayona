using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIdleStateRotate : EnemyBaseState
{
    public float rotationAngle; //in degrees
    public float rotationSpeed; // in seconds
    public float waitTime;
    float stepAngle;
    float timer;
    float degreesTurned;
    float lastAnimation;
    float animation;
    Vector3 originalPos;

    private void Start()
    {
        stepAngle = rotationAngle / rotationSpeed;
        originalPos = transform.position;
        
    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        enemy.animator.SetBool("EnemyMove", false);
        timer = -waitTime;
        degreesTurned = 0;
        lastAnimation = 0;
    }

    float Bezier(float t)
    {
        return -6f * (t-1f) * t;
    }

    public static float EaseIn(float t)
    {
        return Mathf.Pow(t, 3);
    }
    public static float Flip(float x)
    {
        return 1 - x;
    }

    public static float Spike(float t)
    {
        if (t <= .5f)
            return EaseIn(t / .5f);

        return EaseIn(Flip(t) / .5f);
    }
    public override void UpdateState(EnemyStateAgent enemy)
    {
        timer += Time.deltaTime;
        if (timer >= 0 && timer <= rotationSpeed)
        {
            animation = Mathf.Lerp(0, rotationAngle, EaseIn(timer / rotationSpeed));
            transform.Rotate(0, animation - lastAnimation, 0);
            lastAnimation = animation;
        }
        else if (timer > rotationSpeed && timer <= (rotationSpeed * 3))
        {
            animation = Mathf.Lerp(rotationAngle, -rotationAngle, EaseIn((timer - rotationSpeed) / (rotationSpeed * 2)));
            transform.Rotate(0, animation - lastAnimation, 0);
            lastAnimation = animation;
        }
        //else if (timer >= (rotationSpeed * 3) && timer <= (rotationSpeed * 4))
        //{
        //    animation = Mathf.Lerp(-rotationAngle, 0, EaseIn((timer - 3 * rotationSpeed) / (rotationSpeed)));
        //    transform.Rotate(0, animation - lastAnimation, 0);
        //    lastAnimation = animation;
        //}
        else if (timer >= 0)
        {

            enemy.SwitchState(enemy.patrolState);

        }
    }
}
