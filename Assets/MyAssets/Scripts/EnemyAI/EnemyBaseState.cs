using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBaseState: MonoBehaviour
{
    public abstract void EnterState(EnemyStateAgent enemy);
    public abstract void UpdateState(EnemyStateAgent enemy);

}
