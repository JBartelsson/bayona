using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace System.Runtime.CompilerServices
{
    internal static class IsExternalInit { }
}
public class EnemyPatrolStateRotate : EnemyBaseState
{
    public float rotationAngle; //in degrees
    public float rotationSpeed; // in seconds
    public float waitTime;
    Vector3 originalPosition;
    float originalRotation;
    bool animationReverse;
    float stepAngle;
    float timer;
    float degreesTurned;
    float lastAnimation;
    float anim;
    NavMeshAgent agent;

    record AnimationFrame(float Amount, float Seconds);

    private void Awake()
    {
        stepAngle = rotationAngle / rotationSpeed;
        animationReverse = false;
        originalPosition = transform.position;
        agent = GetComponent<NavMeshAgent>();
        originalRotation = transform.rotation.eulerAngles.y;
        
    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        enemy.animator.SetBool("EnemyMove", false);
        timer = -waitTime;
        degreesTurned = 0;
        lastAnimation = 0;
    }

    float Bezier(float t)
    {
        return -6f * (t-1f) * t;
    }

    public static float EaseIn(float t)
    {
        return Mathf.Pow(t, 3);
    }
    public static float Flip(float x)
    {
        return 1 - x;
    }

    public static float Spike(float t)
    {
        if (t <= .5f)
            return EaseIn(t / .5f);

        return EaseIn(Flip(t) / .5f);
    }
    public override void UpdateState(EnemyStateAgent enemy)
    {
        //if (Vector3.Distance(transform.position, originalPosition) > 2)
        //{
        //    agent.SetDestination(originalPosition);
        //    return;
        //}
        timer += Time.deltaTime;
        if (timer >= 0 && timer <= rotationSpeed)
        {
            anim = Mathf.Lerp(0, rotationAngle, EaseIn(timer / rotationSpeed));
            transform.Rotate(0, anim - lastAnimation, 0);
            lastAnimation = anim;
        }
        else if (timer >= 0)
        {
            rotationAngle *= -1;
            enemy.SwitchState(enemy.idleState);

        }
    }
}
