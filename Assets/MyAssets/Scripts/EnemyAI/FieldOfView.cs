using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{

    public float viewRadius;
    public float aroundRadius;
    [Range(0, 360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    public List<Transform> visibleTargets = new();


    public MeshFilter viewMeshFilter;
    public int edgeResolveIterations;
    public float edgeDistanceThreshold;
    Mesh viewMesh;


    public float meshResolution;
    void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;
        //float height = GetComponent<MeshRenderer>().bounds.size.y;
        //viewMeshFilter.transform.position -= new Vector3(0, height / 2 - 0.1f);
        StartCoroutine("FindTargetsWithDelay", .1f);
    }


    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }
    void LateUpdate()
    {
        DrawFieldOfView();
    }
    void FindVisibleTargets()
    {
        void ApplySee(Transform target)
        {
            if (target.CompareTag("Player"))
            {
                visibleTargets.Add(target);
                GameManager.Instance.addAttention(50, TypesOfDeath.Seen);
            }
            if (target.CompareTag("Key"))
            {
                MagicStone scriptCheck = target.GetComponent<MagicStone>();
                if (scriptCheck.isPickedUp)
                {
                    visibleTargets.Add(target);
                    GameManager.Instance.addAttention(25, TypesOfDeath.Key);
                }
            }
            if (target.name == "DoorTriggerObject")
            {
                Door scriptCheckDoor = target.parent.parent.Find("Door").GetComponent<Door>();
                if (scriptCheckDoor != null)
                {
                    if (scriptCheckDoor.originalOpenState != scriptCheckDoor.isOpen)
                    {
                        visibleTargets.Add(target);
                        GameManager.Instance.addAttention(25, TypesOfDeath.Door);
                    }
                }
            }
        }
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        visibleTargets.Clear();
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;



            Vector3 dirToTarget = new Vector3(
                (target.position.x - transform.position.x), 
                0,
                target.position.z - transform.position.z
                );
            float viewAngleTemp = Vector3.Angle(transform.forward, dirToTarget);

            if (Vector3.Distance(transform.position, target.position) < aroundRadius)
            {
                ApplySee(target);
            } else if (viewAngleTemp  < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask)) {

                    ApplySee(target);
                    
                }

            }

        }
        //foreach (var item in visibleTargets)
        //{
        //    Debug.Log(item.name);
        //}
        //Debug.Log("----------------------------------");


    }



    void DrawFieldOfView()
    {
        float coverAngle = 360;
        int stepCount = Mathf.RoundToInt(coverAngle * meshResolution);
        float stepAngleSize = coverAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();

        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i <= stepCount; i++)
        {
            float addedAngle =  stepAngleSize * i;
            float angle = transform.eulerAngles.y - viewAngle / 2 + addedAngle;
            float magnitude;
            ViewCastInfo newViewCast;
            if (addedAngle < viewAngle)
            {
                magnitude = viewRadius;
            } else
            {
                magnitude = aroundRadius;
            }
            newViewCast = ViewCast(angle, magnitude);




            if ((i > 0) && (stepAngleSize * (i - 1) > viewAngle))
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDistanceThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit  && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast, magnitude);
                    if (edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }
            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
     
        viewMesh.RecalculateNormals();
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast, float magnitude)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle, magnitude);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDistanceThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    ViewCastInfo ViewCast(float globalAngle, float magnitude)
    {
        Vector3 direction = DirectionFromAngle(globalAngle, true);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, magnitude, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        } else
        {
            return new ViewCastInfo(false, transform.position + direction * magnitude, magnitude, globalAngle);
        }
    }


    public Vector3 DirectionFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }

}
