using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrolState : EnemyBaseState
{
    public List<Transform> positions;
    List<Vector3> destinations;
    int index;
    NavMeshAgent agent;
    private void Awake()
    {
        destinations = new List<Vector3>
        {
            transform.position
        };
        foreach (Transform item in positions)
        {
            destinations.Add(item.position);
        }
        index = 0;
        agent = GetComponent<NavMeshAgent>();
    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        enemy.animator.SetBool("EnemyMove", true);
        agent.destination = destinations[index];
    }

    

    public override void UpdateState(EnemyStateAgent enemy)
    {
        Vector3 ownPosNoY = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 targetPosNoY = new Vector3(destinations[index].x, 0, destinations[index].z);
        if (Vector3.Distance(ownPosNoY, targetPosNoY) < 1)
        {
            index++;
            index %= (positions.Count + 1);
            agent.SetDestination(transform.position);
            enemy.SwitchState(enemy.idleState);
        }
        
    }
}
