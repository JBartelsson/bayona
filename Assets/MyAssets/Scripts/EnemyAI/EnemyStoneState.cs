using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStoneState : EnemyBaseState
{
    NavMeshAgent agent;
    float timer;
    public float wait;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        agent.SetDestination(enemy.stonePosition);
        timer = Time.time; ;
    }

    public override void UpdateState(EnemyStateAgent enemy)
    {
        if (agent.remainingDistance == 0 || Time.time - timer > wait)
        {
            agent.SetDestination(transform.position);
            enemy.SwitchState(enemy.returnState);
        }
    }

}
