using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateAgent : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector]
    public EnemyBaseState currentState;
    public EnemyBaseState patrolState;
    public EnemyBaseState watchState;
    public EnemyBaseState stoneState;
    public EnemyBaseState idleState;
    public EnemyReturnState returnState;
    public FieldOfView fov;

   
    public Animator animator;

    public Vector3 stonePosition;



    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        currentState = patrolState;
        currentState.EnterState(this);
    }

    // Update is called once per frame
    void Update()
    {

        if (fov.visibleTargets.Count > 0)
        {
            SwitchState(watchState);
        }
        currentState.UpdateState(this);

    }

    
    public void SwitchState(EnemyBaseState state)
    {
        if (state == stoneState)
        {
            animator.SetBool("EnemyMove", true);
        }
        currentState = state;
        state.EnterState(this);
    }
}
