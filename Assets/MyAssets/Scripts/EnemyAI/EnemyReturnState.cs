using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyReturnState : EnemyBaseState
{
    Transform originalTransform;
    Quaternion originalRotation;
    NavMeshAgent agent;

    private void Awake()
    {
        originalTransform = Instantiate(new GameObject(),transform.position, transform.rotation).transform;
        agent = GetComponent<NavMeshAgent>();

    }
    public override void EnterState(EnemyStateAgent enemy)
    {
        agent.destination = originalTransform.position;
    }


    public override void UpdateState(EnemyStateAgent enemy)
    {
        if (agent.remainingDistance == 0)
        {
            agent.destination = transform.position;
            if (Vector3.Angle(originalTransform.forward, transform.forward) > 3)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, originalTransform.rotation, 2 * Time.deltaTime);
            } else
            {
                transform.rotation = originalTransform.rotation;
                enemy.SwitchState(enemy.patrolState);
            }
        }
    }
}
