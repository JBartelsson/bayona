using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstRadius : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector] public float radius;
    public float burstVelocity;
    float burstTime;
    bool start = false;
    float time;
    Color originalColor;
    void Start()
    {
    }

    public void startAnimation()
    {
        time = 0;
        burstTime = radius / burstVelocity;
        originalColor = transform.Find("Sphere").GetComponent<MeshRenderer>().material.color;
        start = true;
    }

    public static float EaseIn(float t)
    {
        return Mathf.Pow(t, 3);
    }
    public static float EaseOut(float t)
    {
        return Flip(Square(Flip(t)));
    }
    static float Flip(float x)
    {
        return 1 - x;
    }
    static float Square(float x)
    {
        return Mathf.Pow(x, 2);
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            time += Time.deltaTime;
            float value = Mathf.Lerp(0, radius, EaseOut( time / burstTime));
            transform.localScale = new Vector3(1, 1, 1) * value;
            float alpha = Mathf.Lerp(originalColor.a, 0, EaseOut(time / burstTime));
            alpha = Mathf.Round(alpha * 1000f) / 1000f;
            Color targetColor = new Color(originalColor.r, originalColor.g, originalColor.b, 0f);
            Color newColor = Color.Lerp(originalColor, targetColor, EaseOut(time / burstTime));
            transform.Find("Sphere").GetComponent<MeshRenderer>().material.color = newColor ;
            if (time > burstTime)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
