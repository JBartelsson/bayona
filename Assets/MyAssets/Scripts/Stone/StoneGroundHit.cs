using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneGroundHit : MonoBehaviour
{
    bool hit;
    public float radius;
    public LayerMask enemyMask;
    public GameObject burstRadius;
    private void Start()
    {
        hit = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("oz");
        if (!hit && !collision.transform.CompareTag("Stone"))
        {
            Collider[] enemies = Physics.OverlapSphere(transform.position, radius, enemyMask);
            foreach (var enemy in enemies)
            {
                EnemyStateAgent stateAgent = enemy.GetComponent<EnemyStateAgent>();
                stateAgent.stonePosition = transform.position;
                stateAgent.SwitchState(stateAgent.stoneState);
            }

            GameObject burst = Instantiate(burstRadius, transform.position, Quaternion.identity);
            burst.GetComponent<BurstRadius>().radius = radius*2;
            burst.GetComponent<BurstRadius>().startAnimation();
            GameManager.Instance.addAttention(20, TypesOfDeath.Stone);
            hit = true;
            
        }
    }
}
