using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ThrowStone : MonoBehaviour
{
    [Header("References")]
    public Transform cam;
    public Transform attackPoint;
    public GameObject stoneObject;
    public GameObject keyObject;

    [Header("Settings")]
    public float throwCooldown;

    [Header("Throwing")]
    public KeyCode stoneKey = KeyCode.Q;
    public KeyCode keyThrowKey = KeyCode.R;
    public float throwForce;

    bool readyToThrow;

    private void Start()
    {
        readyToThrow = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(stoneKey) && readyToThrow && GameManager.Instance.stones != 0 && GameManager.Instance.playerAbilities.Contains(PlayerAbilities.Stone))
        {
            Throw(stoneObject);
            GameManager.Instance.stones -= 1;   
        }
        if (Input.GetKeyDown(keyThrowKey) && readyToThrow && GameManager.Instance.inventory.Count > 0)
        {
            Throw(keyObject);
            GameManager.Instance.inventory.Clear();
        }
    }

    private void Throw(GameObject objectToThrow)
    {
        readyToThrow = false;

        // instantiate object to throw
        GameObject projectile = Instantiate(objectToThrow, cam.position + cam.transform.forward.normalized * 5, cam.rotation);

        // get rigidbody component
        Rigidbody projectileRb = projectile.GetComponent<Rigidbody>();

        // calculate direction
        Vector3 forceDirection = cam.transform.forward;

        RaycastHit hit;

        //if (Physics.Raycast(cam.position, cam.forward, out hit, 500f))
        //{
        //    forceDirection = (hit.point - attackPoint.position).normalized;
        //}

        // add force
        //Vector3 forceToAdd = forceDirection * throwForce + transform.up * throwUpwardForce;
        Vector3 forceToAdd = forceDirection * throwForce;

        projectileRb.AddForce(forceToAdd, ForceMode.Impulse);


        // implement throwCooldown
        Invoke(nameof(ResetThrow), throwCooldown);
    }

    private void ResetThrow()
    {
        readyToThrow = true;
    }
}