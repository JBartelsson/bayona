using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float movementSpeed;
    public float movementTime;
    public float rotationAmount;

    public Vector3 newPosition;

    //HandleChangingCameras
    public Camera FirstPersonCam, ThirdPersonCam;
    public bool fpc;

    //HandleRotation
    public Quaternion newRotation;

    public Vector3 rotateStartPosition;
    public Vector3 rotateCurrentPosition;

    // Start is called before the first frame update
    void Start()
    {
        newPosition = transform.position;
        newRotation = transform.rotation;
        FirstPersonCam.gameObject.SetActive(fpc);
        ThirdPersonCam.gameObject.SetActive(!fpc);

        /*cameraFirstPerson.enabled = true;
        cameraThirdPerson.enabled = false;*/
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            fpc = !fpc;
            FirstPersonCam.gameObject.SetActive(fpc);
            ThirdPersonCam.gameObject.SetActive(!fpc);
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            transform.position = player.transform.position;
            transform.LookAt(transform.position + player.transform.forward);
            foreach (MonoBehaviour script in player.GetComponentsInChildren<MonoBehaviour>())
            {
                script.enabled = fpc;
            }
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        //HandleMovementInput();
        //HandleMouseInput();
    }

    void HandleMouseInput()
    {
        if (Input.GetMouseButtonDown(2))
        {
            rotateStartPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(2))
        {
            rotateCurrentPosition = Input.mousePosition;

            Vector3 difference = rotateStartPosition - rotateCurrentPosition;

            rotateStartPosition = rotateCurrentPosition;

            newRotation *= Quaternion.Euler(Vector3.up * (-difference.x / 5f));
        }
    }

    void HandleMovementInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * movementTime);
    }
}
