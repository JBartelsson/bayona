using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRadiusDetect : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        //if (other.CompareTag("MagicStone"))
        //{
        //    if (checkIfOnGround(other.transform))
        //    {
        //        Debug.Log("Rotation is zero");
        //        MagicStoneGroundHit stone = other.GetComponent<MagicStoneGroundHit>();
        //    if (stone.speed == 0 && stone.wasThrown && stone.rotationSpeed == 0) {
        //        transform.GetComponentInParent<MagicStone>().SuccessfulHit(other.transform);
        //        Destroy(other.gameObject);
                
        //    }
        //    }
        //}
    }

    bool checkIfOnGround(Transform t)
    {
        float faces = 0;
        //Mathf.Abs(Vector3.Angle(t.forward, Vector3.forward)) < 1 || Mathf.Abs(Vector3.Angle(t.forward, -Vector3.forward)) < 1
        float forward = Vector3.Angle(t.forward, Vector3.forward);
        if ( forward < 1 || forward > 179)
        {
            faces++;
        }
        float right = Vector3.Angle(t.right, Vector3.right);
        if (right < 1 || right > 179)
        {
            faces++;
        }
        float up = Vector3.Angle(t.up, Vector3.up);
        if (up < 1 || up > 179)
        {
            faces++;
        }

        if (faces >= 1)
        {
            return true;
        }
        
        return false;
    }
}
