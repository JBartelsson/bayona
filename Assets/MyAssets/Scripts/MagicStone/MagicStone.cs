using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicStone : MonoBehaviour
{
    public string keyID;
    public bool isPickedUp = false;
    public float radius;
    public Material pickedUpMaterial;
    public GameObject throwInRadius;
    Renderer rend;

    Material originalMaterial;

    private void Start()
    {
        throwInRadius.SetActive(false);
        throwInRadius.transform.localScale = new Vector3(radius, throwInRadius.transform.localScale.y, radius) ;
        rend = transform.Find("rust_key").GetComponent<Renderer>();
        originalMaterial = rend.material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isPickedUp && other.CompareTag("Player"))
        {
            isPickedUp = true;
            //transform.eulerAngles = new Vector3(0, 0, 0);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            rend.sharedMaterial = pickedUpMaterial;
            throwInRadius.SetActive(true);
            TriggerBoxColliders(false);
            GameManager.Instance.inventory.Add(keyID);
        }
    }

    void TriggerBoxColliders(bool state)
    {
        BoxCollider box = GetComponents<BoxCollider>()[1];
        box.enabled = state;
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = state;
    }

    public void SuccessfulHit(Transform _transform)
    {
        GetComponent<ParticleSystem>().Play();
        this.transform.SetPositionAndRotation(_transform.position, _transform.rotation);
        throwInRadius.SetActive(false);
        GameManager.Instance.inventory.Remove(keyID);
        TriggerBoxColliders(true);
        isPickedUp = false;
        rend.sharedMaterial = originalMaterial;
        
    }
}
