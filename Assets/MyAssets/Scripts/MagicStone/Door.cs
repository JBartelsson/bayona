using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
    public string doorKey;
    public bool originalOpenState;
    public bool isOpen = false;
    [SerializeField]
    private bool isRotatingDoor = true;
    [SerializeField]
    private float speed = 1f;
    [Header("Rotation Configs")]
    [SerializeField]
    private float rotationAmount = 90f;
    [SerializeField]
    private float forwardDirection = 0;
    [Header("Sliding Configs")]
    [SerializeField]
    private Vector3 slideDirection = Vector3.back;
    [SerializeField]
    private float slideAmount = 1.9f;


    private Vector3 startRotation;
    private Vector3 startPosition;
    private Vector3 forward;

    private Coroutine animationCoroutine;
    LayerMask ignore;

    RaycastHit data;
    private void Update()
    {
        Camera main = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Camera>();
        if (main != null)
        {
        Ray ray = main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Debug.DrawRay(ray.origin, ray.direction * 10);
        if (Input.GetKeyDown(KeyCode.E))
        {
            {
                if (GameManager.Instance.inventory.Contains(doorKey))
                {
                    
                    if (Physics.Raycast(ray, out data, 10f, ignore))
                    {
                        if (data.transform.name == "Door")
                        {
                            toggleDoor();
                        }
                    }

                }
            }
        }
        }
    }

    void toggleDoor()
    {
        if (!isOpen)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void Start()
    {
        originalOpenState = isOpen;
        ignore = LayerMask.GetMask("Obstacles");
    }
    private void Awake()
    {
        startRotation = transform.rotation.eulerAngles;
        // Since "Forward" actually is pointing into the door frame, choose a direction to think about as "forward" 
        forward = transform.right;
        startPosition = transform.position;
    }

    public void Open()
    {
        if (!isOpen)
        {
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
            }

            if (isRotatingDoor)
            {
                
                animationCoroutine = StartCoroutine(DoRotationOpen());
            }
            else
            {
                animationCoroutine = StartCoroutine(DoSlidingOpen());
            }
        }
    }

    private IEnumerator DoRotationOpen()
    {
        Quaternion startRotation = transform.rotation;
        Quaternion endRotation;

        endRotation = Quaternion.Euler(new Vector3(0, this.startRotation.y - rotationAmount, 0));
        
        isOpen = true;

        float time = 0;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(startRotation, endRotation, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
    }

    private IEnumerator DoSlidingOpen()
    {
        Vector3 endPosition = this.startPosition + slideAmount * slideDirection;
        Vector3 startPosition = transform.position;

        float time = 0;
        isOpen = true;
        while (time < 1)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
    }

    public void Close()
    {
        if (isOpen)
        {
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
            }

            if (isRotatingDoor)
            {
                animationCoroutine = StartCoroutine(DoRotationClose());
            }
            else
            {
                animationCoroutine = StartCoroutine(DoSlidingClose());
            }
        }
    }

    private IEnumerator DoRotationClose()
    {
        Quaternion startRotation = transform.rotation;
        Quaternion endRotation = Quaternion.Euler(this.startRotation);

        isOpen = false;

        float time = 0;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(startRotation, endRotation, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
    }

    private IEnumerator DoSlidingClose()
    {
        Vector3 endPosition = this.startPosition;
        Vector3 startPosition = transform.position;
        float time = 0;

        isOpen = false;

        while (time < 1)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
    }
}