using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicStoneGroundHit : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    public bool wasThrown;
    public float minSpeed;
    public float minRotationSpeed;
    Rigidbody rigidbody;
    GameObject sphereHit;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        wasThrown = false;
        sphereHit = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "ThrowInRadius")
        {
            sphereHit = other.gameObject;
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "ThrowInRadius")
        {
            sphereHit = null;
        }
    }
    private void Update()
    {

        speed = rigidbody.velocity.magnitude;
        rotationSpeed = rigidbody.angularVelocity.magnitude;
        if (speed > 0.5 && !wasThrown)
        {
            wasThrown = true;
        }
        if (speed < minSpeed && wasThrown)
        {
            rigidbody.velocity = new Vector3(0, 0, 0);
            speed = 0;
        }
        //if (rotationSpeed <minRotationSpeed)
        //{
        //    rigidbody.angularVelocity = new Vector3(0, 0, 0);
        //}
        //Debug.Log(speed);
        //Debug.Log(rotationSpeed);
        if (speed == 0 && wasThrown)
        {
            if (sphereHit != null)
            {
                sphereHit.GetComponentInParent<MagicStone>().SuccessfulHit(transform);
                
            }
            Destroy(this.gameObject);
        }


    }
}
