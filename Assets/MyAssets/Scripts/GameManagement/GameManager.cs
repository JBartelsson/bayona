using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum TypesOfDeath
{
    Seen,
    Key,
    Door,
    Stone
}

public enum PlayerAbilities
{
    Key,
    Stone
}
public enum GameState
{
    Pause,
    Playing,
    StartMenu,
    GameOver,
    Help,
    Won,
    Finished,
    Save
}
public enum DialogState
{
    AbleTo,
    During,
    Triggered,
    None
}

public class GameManager : Singleton<GameManager>
{

    public float timeInSec = 0;
    public List<string> inventory;
    public List<Transform> respawnPositions;

    public List<int> startStoneAmount;
    public int stones;
    int lastStones;

    int attention;
    public int maxAttention;
    public TypesOfDeath deathReason;
    public Vector3 respawnPoint;

    public List<PlayerAbilities> playerAbilities = new();
    public DialogState dialogState = DialogState.None;

    public GameState gameState = GameState.Playing;


    //statistics
    public int deaths;
    
    public int currentLevel;

    bool isPaused = false;


    // Update is called once per frame
    void Update()
    {
        if (gameState == GameState.Playing)
        {
            timeInSec += Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            TriggerPause();
        }
        if (attention > maxAttention)
        {
            attention = 0;
            GameOver();
            
        }
        
    }

    public void ActivateStones()
    {
        playerAbilities.Add(PlayerAbilities.Stone);
    }

    public void NextLevel()
    {
        currentLevel++;
        
        SetRespawnPosition(respawnPositions[currentLevel - 1].position);
        lastStones = startStoneAmount[currentLevel - 1];
        timeInSec = 0;
        deaths = 0;
        RestartLevel();
    }

    public void RestartLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (gameState != GameState.StartMenu)
        {
            Init();
        }
    }


    private void Init()
    {
        inventory = new List<string>();
        stones = lastStones;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
        Vector3 pos;
        if (respawnPoint == null)
        {
            pos = respawnPositions[currentLevel - 1].position;
        } else
        {
            pos = respawnPoint;
        }
        GameObject.FindGameObjectWithTag("Player").transform.position = pos;
        attention = 0;
        Time.timeScale = 1;
        gameState = GameState.Playing;
    }
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
     
    }
    public void InitPlaying()
    {
        gameState = GameState.Playing;
        SceneManager.sceneLoaded += OnSceneLoaded;
        SetRespawnPosition(respawnPositions[currentLevel - 1].position);
        lastStones = startStoneAmount[currentLevel - 1];
        Init();
    }

    private void TriggerPause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            Time.timeScale = 0;
            gameState = GameState.Pause;
        } else
        {
            Time.timeScale = 1;
            gameState = GameState.Playing;
        }
    }


    public void SetRespawnPosition(Vector3 tr)
    {
        if (!CheckWatchState())
        {
            respawnPoint = tr;
            lastStones = stones;
            gameState = GameState.Save;
        }
    }

    public void GameOver()
    {
        gameState = GameState.GameOver;
        deaths++;
        Time.timeScale = 0;
    }

    bool CheckWatchState()
    {
        EnemyStateAgent[] enemies = FindObjectsOfType<EnemyStateAgent>();
        foreach (var enemy in enemies)
        {
            if (enemy.currentState == enemy.watchState)
            {
                return true;
            }
        }
        return false;
    }
    public void LevelFinished()
    {
        if (!CheckWatchState())
        {
            Time.timeScale = 0;
            if (!(currentLevel == respawnPositions.Count))
            {
                gameState = GameState.Finished;
            } else
            {
                gameState = GameState.Won;
            }
        }
    }

    public void addAttention(int amount, TypesOfDeath reason)
    {
        attention += amount;
        deathReason = reason;
    }
    public int Attention
    {
        get => attention;
        set => attention = value;
    }

    public string GetTimeinString()
    {
        var minutes = timeInSec / 60; //Divide the guiTime by sixty to get the minutes.
        var seconds = timeInSec % 60;//Use the euclidean division for the seconds.
        var fraction = (timeInSec * 100) % 100;

        //update the label value
        //return string.Format("{0:00} : {1:00}", minutes, seconds);
        return string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);
    }

}