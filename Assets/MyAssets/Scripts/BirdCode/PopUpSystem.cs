using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class PopUpSystem : MonoBehaviour
{
    [SerializeField] GameObject dialogBox;
    public Animator animator;
    [SerializeField] TMP_Text dialogText;

    [SerializeField] int lettersPerSecond;

    public event Action OnShowDialog;
    public event Action OnCloseDialog;


    public static PopUpSystem Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    Dialog dialog;
    int currentLine = 0;
    bool isTyping;

    public IEnumerator ShowDialog(Dialog dialog)
    {
        yield return new WaitForEndOfFrame();
        OnShowDialog?.Invoke();
        foreach (var line in dialog.Lines)
        {
            Debug.Log(line);
        }

        this.dialog = dialog;
        dialogBox.SetActive(true);
        StartCoroutine(TypeDialog(dialog.Lines[0]));
        
        animator.SetTrigger("pop");
    }

    public void Update()
    {
        if (Input.GetKeyDown("z") && !isTyping && GameManager.Instance.dialogState == DialogState.During)
        {
            ++currentLine;
            if (currentLine < dialog.Lines.Count)
            {
                StartCoroutine(TypeDialog(dialog.Lines[currentLine]));
            }
            else
            {
                Debug.Log("Ended");
                currentLine = 0;
                dialogBox.SetActive(false);
               
                OnCloseDialog?.Invoke();
            }
        }
    }

    public IEnumerator TypeDialog(string line)
    {
        isTyping = true;
        dialogText.text = "";
        foreach (var letter in line.ToCharArray())
        {
            dialogText.text += letter;
            yield return new WaitForSeconds(1f / lettersPerSecond);
        }
        isTyping = false;
    }
    public void Start()
    {
        PopUpSystem.Instance.OnShowDialog += () =>
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<FirstPersonMovement>().enabled = false;
            GameManager.Instance.dialogState = DialogState.During;
        };
        PopUpSystem.Instance.OnCloseDialog += () =>
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<FirstPersonMovement>().enabled = true;
            GameManager.Instance.dialogState = DialogState.None;
            Debug.Log("End of Dialog");
        };
    }
}
