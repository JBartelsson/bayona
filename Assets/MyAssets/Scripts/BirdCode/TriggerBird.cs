using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBird : MonoBehaviour
{
    [SerializeField] Dialog dialog;

    
    public void Interact()
    {
        StartCoroutine(PopUpSystem.Instance.ShowDialog(dialog));
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (GameManager.Instance.dialogState == DialogState.None)
            {
                GameManager.Instance.dialogState = DialogState.AbleTo;
                
            }
            if (GameManager.Instance.dialogState == DialogState.AbleTo)
            {
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    GameManager.Instance.dialogState = DialogState.Triggered;
                    Interact();     
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.dialogState = DialogState.None;
        }
    }

    private void Start()
    {
        
    }
}
