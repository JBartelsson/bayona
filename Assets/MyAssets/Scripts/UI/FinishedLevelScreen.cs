using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class FinishedLevelScreen : MonoBehaviour
{
    public TMP_Text statistics;
    public void Setup()
    {
        gameObject.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        statistics.text = "Time: " + GameManager.Instance.GetTimeinString() + "\n" + "Deaths: " + GameManager.Instance.deaths;
    }

    public void NextLevel ()
    {
        GameManager.Instance.NextLevel();
    }

    public void MenuButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void OptionButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(3);
    }
}
