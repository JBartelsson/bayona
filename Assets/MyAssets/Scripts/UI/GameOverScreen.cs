using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverScreen : MonoBehaviour
{
    // Start is called before the first frame update

    public string mainMenuSceneName;
    public TMP_Text deathFeedbackText;

    public void Setup()
    {
        gameObject.SetActive(true);
        string text;
        switch (GameManager.Instance.deathReason)
        {
            case TypesOfDeath.Seen:
                text = "You've been seen by a watcher";
                break;
            case TypesOfDeath.Key:
                text = "A watcher noticed a missing key";
                break;
            case TypesOfDeath.Door:
                text = "A door attracted attention";
                break;
            case TypesOfDeath.Stone:
                text = "Somebody saw your stone";
                break;
            default:
                text = "Don't draw attention!";
                break;
        }
        deathFeedbackText.text = text;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void RestartButton()
    {
        GameManager.Instance.RestartLevel();
    }

    public void MenuButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

   

}
