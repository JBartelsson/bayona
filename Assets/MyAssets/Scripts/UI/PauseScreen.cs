using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    //public TMP_Text statistics;
    public void Setup(bool status)
    {
        gameObject.SetActive(status);
       

    }

    public void HelpButton()
    {
        GameManager.Instance.gameState = GameState.Help;
    }
    public void RestartButton()
    {
        GameManager.Instance.RestartLevel();
    }
    public void MenuButton()
    {
        Time.timeScale = 1;
        GameManager.Instance.gameState = GameState.StartMenu;
        SceneManager.LoadScene(0);
    }
}
