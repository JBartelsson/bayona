using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class HUDManager : MonoBehaviour
{
    // Start is called before the first frame update
    public TMP_Text timerLabel;
    public Slider attentionSlider;
    public GameOverScreen gameOverScreen;
    public FinishedLevelScreen finishedLevelScreen;
    public RawImage keyReference;
    public GameObject stoneLabel;
    public WinningScreen winningScreen;
    public PauseScreen pause;
    public GameObject dialogHelp;
    public HelpScreen help;
    public GameObject saveIndicator;
    void Start()
    {
        attentionSlider.maxValue = GameManager.Instance.maxAttention;
    }

    // Update is called once per frame

    void SetCursor(bool state)
    {
        Cursor.visible = state;
        if (state)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    IEnumerator ShowAndHide(GameObject go, float delay)
    {
        go.SetActive(true);
        yield return new WaitForSeconds(delay);
        go.SetActive(false);
    }
    void Update()
    {
        if (GameManager.Instance.gameState == GameState.Save)
        {
            StartCoroutine(ShowAndHide(saveIndicator, 2.0f));
            GameManager.Instance.gameState = GameState.Playing;
        }
        if (GameManager.Instance.gameState == GameState.Playing)
        {
            pause.Setup(false);
            SetCursor(false);

        } else
        {
            SetCursor(true);
        }
        if (GameManager.Instance.gameState == GameState.Pause)
        {
            pause.Setup(true);
        }
        if (GameManager.Instance.gameState == GameState.GameOver)
        {
            gameOverScreen.Setup();
        }
        if (GameManager.Instance.gameState == GameState.Help)
        {
            help.Setup(true);
        } else
        {
            help.Setup(false);
        }
        if (GameManager.Instance.gameState == GameState.Finished)
        {
            finishedLevelScreen.Setup();
        }
        if (GameManager.Instance.gameState == GameState.Won)
        {
            winningScreen.Setup();
        }

        attentionSlider.value = GameManager.Instance.Attention;

        timerLabel.text = GameManager.Instance.GetTimeinString();

        bool stoneState = GameManager.Instance.playerAbilities.Contains(PlayerAbilities.Stone);
        stoneLabel.SetActive(stoneState);
        if (stoneState)
        {
            stoneLabel.transform.GetComponentInChildren<TMP_Text>().text = GameManager.Instance.stones.ToString();
        }

        if (GameManager.Instance.inventory.Count > 0)
        {
            keyReference.gameObject.SetActive(true);
        } else
        {
            keyReference.gameObject.SetActive(false);
        }
        if (!saveIndicator.active)
        {
            dialogHelp.SetActive(GameManager.Instance.dialogState == DialogState.AbleTo);
        }
    }

    
}
