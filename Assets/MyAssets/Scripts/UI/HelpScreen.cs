using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class HelpScreen : MonoBehaviour
{
    //public TMP_Text statistics;
    public void Setup(bool state)
    {
        gameObject.SetActive(state);

    }

    public void MenuButton()
    {
        GameManager.Instance.gameState = GameState.Pause;
    }
}
