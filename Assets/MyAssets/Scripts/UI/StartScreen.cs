using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    public void NewGameButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
}
