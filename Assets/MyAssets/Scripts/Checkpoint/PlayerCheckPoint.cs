using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheckPoint : MonoBehaviour
{
   public int addStones;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (GameManager.Instance.respawnPoint != transform.position)
            {
                GameManager.Instance.stones += addStones;
            }
                GameManager.Instance.SetRespawnPosition(transform.position);
        }
    }
}
