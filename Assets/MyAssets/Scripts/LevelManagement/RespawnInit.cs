using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.Instance.respawnPositions.Count == 0)
        {
            int i = 0;
            foreach (Transform item in transform.GetComponentsInChildren<Transform>())
            {
                if (i != 0)
                {
                    GameManager.Instance.respawnPositions.Add(item);
                }
                i++;
            }
            GameManager.Instance.InitPlaying();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
