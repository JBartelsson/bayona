using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide3D : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> levels;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == GameState.Playing)
        {
            for (int i = 0; i < levels.Count; i++)
            {
                if (i == (GameManager.Instance.currentLevel - 1))
                {
                    levels[i].SetActive(true);
                } else
                {
                    levels[i].SetActive(false);
                }
            }
        }
    }
}
